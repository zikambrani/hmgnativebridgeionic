import { Injectable } from '@angular/core';
import { Cordova, IonicNativePlugin, Plugin } from '@ionic-native/core';

/**
 * @name HmgNativeBridge
 * @description
 * hmg native bridge
 *
 * @interfaces
 * HmgNativeBridgeStartupOptions
 */

export interface HmgNativeBridge_INTERFACE_OPTIONS {
  option?: any;
}

@Plugin({
  pluginName: 'HmgNativeBridge',
  plugin: 'hmg-native-bridge',
  pluginRef: 'HmgNativeBridge',
  repo: 'REPO',
  platforms: ['iOS,Android'],
})
@Injectable()
export class HmgNativeBridge extends IonicNativePlugin {
  /**
   * Starts HmgNativeBridge.
   * @param {HmgNativeBridge_INTERFACE_OPTIONS} options
   * @return {Promise<any>}
   */
  @Cordova({
    successIndex: 1,
    errorIndex: 2,
  })
  startPlugin(options: PLUGIN_INTERFACE_OPTIONS): Promise<any> {
    return;
  }
}
